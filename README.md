# next-aws-ecr-ecs-gitlab-ci-cd-example

Example Next.js project with a GitLab CI/CD pipeline that does the following:

- builds a Docker image
- pushes it to Amazon ECR
- deploys it to an Amazon ECS Fargate cluster
- uploads static assets to Amazon S3
 
## Prerequisites

Assumes you have the following:

- Amazon ECR repository
- Amazon ECS Fargate cluster
- Amazon ECS task definition
- Amazon ECS service
- Amazon S3 bucket
- AWS IAM user with ECR, ECS, and S3 permissions

## Usage

- fork the repo
- go to "Settings" > "CI/CD" > "Variables"
- add the following variables:
  - `AWS_ACCESS_KEY_ID`
  - `AWS_SECRET_ACCESS_KEY`
  - `ECR_HOST` (This is the part of the repository URI before the `/`. It looks something like `XXXXXXXXXXXX.dkr.ecr.us-east-1.amazonaws.com`)
- edit the following variables in [`.gitlab-ci.yml`](/.gitlab-ci.yml):
  - `CI_APPLICATION_REPOSITORY`
  - `CI_AWS_ECS_CLUSTER`
  - `CI_AWS_ECS_SERVICE`
  - `CI_AWS_ECS_TASK_DEFINITION`
  - `CI_AWS_S3_BUCKET`
- clone and push a commit
- see the pipeline running in the GitLab UI under "CI/CD" > "Pipelines"
- in a browser, visit the public IP address of the cluster task followed by `:3000` because the server is running on port 3000

## Interesting files

- [`.gitlab-ci.yml`](/.gitlab-ci.yml)
- [`bin/build-and-push-image-to-ecr`](/bin/build-and-push-image-to-ecr)
- [`bin/upload-assets-to-s3`](/bin/upload-assets-to-s3)
- [`bin/ecs`](/bin/ecs) (copied from [GitLab repo](https://gitlab.com/gitlab-org/cloud-deploy/-/blob/master/aws/src/bin/ecs))
- [`Dockerfile`](/Dockerfile)
- [`next.config.js`](/next.config.js)

## References (build/push)
- https://www.youtube.com/watch?v=jg9sUceyGaQ
- https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#docker
- https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#using-docker-caching

## References (deploy)
- https://docs.gitlab.com/ee/ci/cloud_deployment/#deploy-your-application-to-the-aws-elastic-container-service-ecs
- https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/AWS/Deploy-ECS.gitlab-ci.yml
- https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/Deploy/ECS.gitlab-ci.yml
- https://gitlab.com/gitlab-org/cloud-deploy/-/blob/master/aws/ecs/Dockerfile
- https://gitlab.com/gitlab-org/cloud-deploy/-/blob/master/aws/src/bin/ecs
